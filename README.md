# 🌌 Overview

Swag@Home project global documentation, overview and principles

## What is this project?

Swag@Home aims to provide entirely customisable and servicable devices and edge-computing features, respecting as much as possible environment and societal constraints without locking users in vendor specific applications or reducing their capacity to use, customize and repair their devices which generates quick e-waste

## Why this project?

IoT and new technologies such as cloud/edge-computing and metaverse are blooming quickly with numerous solutions and frameworks now for any use-case (cloud data aggregators, smart home boxes, vocal assistants, augmented reality (AR) and virtual reality (VR) apps, ...etc).

However, while there are a wide variety of solutions available they often tend to target a very fast deployment to hit the market and because of this have some big issues regarding Green-IT and RSE even from major companies. For example:

- ❌ Devices often relies on vendor cloud / application protocols and thus become unusable once the company stops the service so it generates e-waste from the many users who aren't able to reverse-engineer them: [Insteon goes out of business](https://siliconangle.com/2022/04/18/home-automation-company-insteon-reportedly-goes-business/)
- ❌ Devices can rely on communication or protocols requiring an external service provider such as 2G/3G/4G/5G/...etc which can't continue to operate once said communication shuts off: [What The 3G Shutdown Means For IoT](https://www.thefastmode.com/expert-opinion/23206-what-the-3g-shutdown-means-for-iot)
- ❌ Development frameworks and boards enable to build completely custom devices and maintain them however it's not accessible for less technology skilled peoples with a portion of the population which as no smartphone, computer or internet access: [Statistics of electronic equipments in households in France since 2004](https://www.insee.fr/fr/statistiques/5057474)
- ❌ Security of IoT devices is variable with a fair portion of them just transmitting in plaintext or with commonly known crendentials
- ❌ Lack of repairability and availability of parts at fair prices as the devices are often designed to be cheap and hit the market first rather than repairable so many devices don't have spare parts available and for some even when the parts are available they can be sold at very high price (for example hundred of euros for a logic board) compared to their manufacture price

## How to solve these issues?

Swag@Home aims to solve these issues by creating yet another development framework (just look at the 2000+ plugins available for [Homebridge](https://homebridge.io/) to see only a portion of how much different devices, protocols and frameworks exists) but instead reusing and recycling as much as possible standards / commonly available tools and components then eventually providing ameliorations for them in order to make smart devices able to help peoples and the planet instead of frustrating them.

This goes by targeting the following key points:

- ✅ Provide full documentation about the device and clean interfaces to use it, enabling complete maintenance and usage of the device without the need of the vendor while eventually protecting the software inside of it if it's proprietary code
- ✅ Reusing, recycling existing common and widely available technologies while eventually bringing optimizations on them
- ✅ Use battle-tested secured standards for features and communication
- ✅ Aims for minimalistic and simple usage for accessibility to anybody while the opened interfaces should enable more advanced users to use the device to do anything they want with it
- ✅ No lock-ins
- ✅ Autonomy of the devices such as : ability to operate by themselves, ability to be repaired by anybody else than the manufacturer and possible ability to generate and store its own required resources (such as energy)
- ✅ Enabling access and improving in-real-life usages by using new technologies such as the metaverse and blockchains in a way to improve life confort and environment instead of burning them by using these technologies to provide features such as the possibility to locate a device, lend it to a friend, sell it or buying it second hand with all its options, protecting against stealing and frand without requiring technology skills and totally independently from the manufacturer

## Global architecture of the project

Swag@Home is composed from the following main types of devices:

- 🛸 Feature devices: Standalone devices that are used to do one or a couple of simple things (e.g. monitoring environment, controlling lights or temperature or something else, ...etc) and can be used more extensively by using their exposed API
- 🖥 Computing devices: Devices used for computing use-cases, preferably in a simplified way when integrated in Swag@Home to be accessible intuitively by anyone for basic usage but it can also be any classic computing device when used for even more extended usages. Computing devices following Swag@Home design also exposes API to remotely control them and aims to be recyclable and maintenable
- ⚙ Extension devices: Components with open interface designed to be used as genericly as possible including by both feature devices and computing devices
